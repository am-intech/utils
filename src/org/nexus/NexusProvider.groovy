package org.nexus

class NexusProvider {

    String baseUrl
    String auth
    
    NexusProvider(String baseUrl, String project, String credsId) {
        this.baseUrl = baseUrl

        def jenkinsCredentials = CredentialsProvider.lookupCredentials(
            Credentials.class,
            Jenkins.instance.getItem(project),
            null,
            null
        );
        def creds = jenkinsCredentials.find {it.id.contains(credsId)}
        this.auth = "$creds.username:$creds.password"
    }

    String[] listComponents(String repo, String component) {
        String url = "${this.baseUrl}/service/rest/v1/search?repository=${repo}&name=${component}"

        String data = ("curl -s -u ${this.auth} -X GET ${url}").execute().text
        def dataJSON = new JsonSlurper().parseText(data)
        def resultItemList = dataJSON.items

        while (dataJSON.continuationToken && dataJSON.continuationToken != 'null') {
            data = ("curl -u ${auth} -X GET ${url}&continuationToken=${continuationToken}").execute().text
            dataJSON = new JsonSlurper().parseText(data)
            resultItemList += dataJSON.items
            continuationToken = dataJSON.continuationToken
        }

        String[] tagList = resultItemList.collect { it.version }
        if (tagList) {
            tagList.sort()
            tagList = tagList.reverse()
        } else {
            return ['no tags available']
        }
        return tagList
    }
}
